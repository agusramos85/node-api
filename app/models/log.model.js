'use strict';
module.exports = function(sequelize, DataTypes) {
    var Log = sequelize.define('Log', {
        idLog: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idUser: {
            type: DataTypes.INTEGER
        },
        action: {
            type: DataTypes.STRING
        }
    },
        {
            freezeTableName: false,
            tableName: 'log',
            timestamps: false,
            classMethods: {
                associate: function(models) {
                    Log.belongsTo(models.User, {foreignKey: 'idUser'});
                }
            }
        });

    return Log;
};
