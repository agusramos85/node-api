'use strict';
module.exports = function(sequelize, DataTypes) {
    var UserProjectHours = sequelize.define('UserProjectHours', {
        idProjectHours: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        idUser: {
            type: DataTypes.STRING,
        },
        idProject: {
            type: DataTypes.STRING
        },
        date: {
            type: DataTypes.DATE
        },
        comments: {
            type: DataTypes.STRING
        },
        hoursAm: {
            type: DataTypes.DECIMAL
        },
        hoursPm: {
            type: DataTypes.DECIMAL
        },
        keyNumber: {
            type: DataTypes.STRING
        }
    },
        {
            freezeTableName: false,
            tableName: 'user_project_hours',
            timestamps: false,
            classMethods: {
                associate: function(models) {
                    UserProjectHours.belongsTo(models.User, {foreignKey: 'idUser'});
                    UserProjectHours.belongsTo(models.Project, {foreignKey: 'idProject'});
                }
            }
        });

    return UserProjectHours;
};
