'use strict';
module.exports = function(sequelize, DataTypes) {
    var Project = sequelize.define('Project', {
        idProject: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            field: 'idProject'
        },
        name: {
            type: DataTypes.STRING,
            field: 'name' // Will result in an attribute that is firstName when user facing but first_name in the database
        },
        idTroi: {
            type: DataTypes.STRING
        }
    },
        {
            freezeTableName: false,
            tableName: 'Projects',
            timestamps: false,
            classMethods: {
                associate: function(models) {
                    Project.belongsToMany(models.User, {through: 'user_projects', foreignKey: 'idProject'});
                }
            }
        });

    return Project;
};
