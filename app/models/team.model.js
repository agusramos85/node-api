'use strict';
module.exports = function(sequelize, DataTypes) {
    var Team = sequelize.define('Team', {
        idTeam: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            field: 'name' // Will result in an attribute that is firstName when user facing but first_name in the database
        },
        description: {
            type: DataTypes.STRING,
            field: 'description'
        }
    },
        {
            freezeTableName: false,
            tableName: 'Teams',
            timestamps: false
        });
    return Team;
};
