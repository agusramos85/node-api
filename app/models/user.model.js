'use strict';
module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        idUsers: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        firstName: {
            type: DataTypes.STRING,
            field: 'firstName'
        },
        lastName: {
            type: DataTypes.STRING,
            field: 'lastName'
        },
        idTeam: {
            type: DataTypes.INTEGER,
            field: 'idTeam'
        },
        idUsersTitle: {
            type: DataTypes.INTEGER,
            field: 'idUsersTitle'
        },
        password: {
            type: DataTypes.STRING,
            field: 'password'
        },
        canLogin: {
            type: DataTypes.BOOLEAN,
            field: 'canLogin'
        },
        email: {
            type: DataTypes.STRING,
            field: 'email'
        },
        tokenSalt: {
            type: DataTypes.STRING,
            field: 'tokenSalt'
        }
    },
        {
            freezeTableName: false,
            tableName: 'Users',
            timestamps: false,
            classMethods: {
                associate: function(models) {
                    User.belongsTo(models.UserTitle, {foreignKey: 'idUsersTitle'});
                    User.belongsTo(models.Team, {foreignKey: 'idTeam'});
                    User.belongsToMany(models.Project, {through: 'user_projects', foreignKey: 'idUsers'});
                }
            }
        });
    return User;
};
