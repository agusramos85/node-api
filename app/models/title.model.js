'use strict';
module.exports = function(sequelize, DataTypes) {
    var UserTitle = sequelize.define('UserTitle', {
        idUsersTitle: {
            type: DataTypes.STRING,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            field: 'name' // Will result in an attribute that is firstName when user facing but first_name in the database
        },
    },
        {
            freezeTableName: false,
            tableName: 'users_title',
            timestamps: false
        });
    return UserTitle;
};
