var models  = require('../models');

exports.getCombo = function(req, res) {
    models.User.findAll(
        {attributes: [['idUsers', 'value'], [models.sequelize.literal('CONCAT(firstName, Char(32), lastName)'), 'label']]}
    ).then(function(users) {
        res.status(200).send(users);
    });
};

exports.get = function(req, res) {
    models.User.findAll(
        {
            include: [models.Team, models.UserTitle],
            attributes: ['firstName', 'lastName', 'idUsers', 'canLogin', 'workingHours',
                'canLogin', 'email']
        }
    ).then(function(users) {
        res.status(200).send(users);
    });
};

exports.getHours = function(req, res) {
    if (!req.params.idUser) return res.status(404).send('User not valid');

    models.UserProjectHours.findAll({
        include: [{model: models.User, attributes: ['firstName', 'lastName', 'email', 'idUsers']},
            models.Project],
        where: { idUser: req.params.idUser }
    }).then((userHours) => {
        res.status(200).send(userHours);
    })
    .catch((e) => {
        console.log(e);
        res.status(500).send('Contact Administrator');
    });
};

exports.postHours = function(req, res) {
    models.UserProjectHours.create(req.body).then((created) => {
        res.status(201).send(created);
    });
};
