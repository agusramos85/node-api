var models  = require('../models');
var dbLogger = require('./../utils/dbLogger');

exports.getCombo = function(req, res) {
    models.Project.findAll({attributes: [['idProject', 'value'], ['name', 'label']]}).then(function(projects){
        res.status(200).send(projects);
    })
    .catch(function(err){
        res.status(500).send(err);
    });
};

exports.get = function(req, res) {
    models.Project.findAll().then(function(projects){
        res.status(200).send(projects);
    })
    .catch(function(err){
        res.status(500).send(err);
    });
};

exports.post = function(req, res) {
    models.Project.create(req.body.project).then((created) => {
        created.addUsers(req.body.users);
        created.save()
        .then(() => {
            res.status(201).send('Created Ok');
        })
        .catch( ()=> {
            res.status(500).send('There was an error');
        });
    });
};

exports.testOrm = function(req, res) {
    models.User.findAll({include: [models.Team, models.UserTitle]}).then(function(users) {
        res.status(200).send(users);
    });
};
