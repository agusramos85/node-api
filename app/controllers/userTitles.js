var models  = require('../models');

exports.get = function(req, res) {
    models.UserTitle.findAll(
        {attributes: [['idUsersTitle', 'value'], ['name', 'label']]}
    ).then(function(userTitles) {
        res.status(200).send(userTitles);
    });
};
