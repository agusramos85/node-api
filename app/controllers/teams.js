var models  = require('../models');

exports.get = function(req, res) {
    models.Team.findAll(
        {attributes: [['idTeam', 'value'], ['name', 'label']]}
    ).then(function(teams) {
        res.status(200).send(teams);
    });
};
