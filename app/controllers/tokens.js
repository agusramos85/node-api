var jwt = require('jsonwebtoken');
var config = require('./../config');
var crypto = require('crypto');
var models  = require('../models');
var passwordHash  = require('password-hash');

exports.get = function(req, res) {
    if (!req.body.email || !req.body.email) {
        res.status(401).send('Wrong user or password');
        return;
    }

    models.User.findAll({where: {email: req.body.email}, plain: true}).then(function(users) {
        if (users === null) {
            return res.status(401).send('Wrong user or password');
        }
        if (passwordHash.verify(req.body.password, users.password)) {
            var hmac = crypto.createHmac('sha256', config.key);
            hmac.update(users.password, users.tokenSalt);
            var sign_hash = hmac.digest('binary');
            var user_result = {
                firstName: users.firstName,
                lastName: users.lastName,
                email: users.email,
                sign: sign_hash,
                canLogin: users.canLogin,
                idUser: users.idUsers
            };
            var token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (600 * 600),
                data: user_result
            }, config.secret);
            delete user_result.sign;
            var data = {
                token: token,
                data: user_result
            };

            return res.status(200).send(JSON.stringify(data));

        }else{
            return res.status(401).send('Wrong user or password');
        }
    });
};
