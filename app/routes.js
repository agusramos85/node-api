var express         = require('express');
var mailSender      = require('./controllers/emailSender.js');
var projectsCtrl    = require('./controllers/projects');
var router          = express.Router();
var teamsCtrl       = require('./controllers/teams.js');
var titlesCtrl      = require('./controllers/userTitles.js');
var tokens          = require('./controllers/tokens.js');
var usersCtrl       = require('./controllers/users');

router.use(function(req, res, next) {
    next();
});

// Test method
router.get('/', function(req, res) {
    res.json({ message: 'works!!!' });
});

router.route('/login')
    .post(tokens.get);

router.route('/users')
    .get(usersCtrl.get);

router.route('/users/combo')
    .get(usersCtrl.getCombo);

router.route('/users/hours/:idUser?')
    .get(usersCtrl.getHours)
    .post(usersCtrl.postHours);

router.route('/projects')
    .get(projectsCtrl.getCombo)
    .post(projectsCtrl.post);

router.route('/send_mail')
    .get(mailSender.send);

router.route('/teams')
    .get(teamsCtrl.get);

router.route('/titles')
    .get(titlesCtrl.get);

module.exports = router;
