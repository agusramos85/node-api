'use strict';
var crypto = require('crypto');
var config = require('./../config');
var jwt = require('jsonwebtoken');
var models = require('../models');
var key = config.key;
var secret = config.secret;

/**
 * Check Token
 * @param {Object} request
 * @param {Object} response
 * @param {Object} next
 * @return {Object} response
 */
module.exports = function checkAuth(req, res, next) {
    if (req.path === '/api/login') return next();

    var token = null;
    if (!req.get('Authorization')) {
        return res.status(401).send('Missing Authorization header');
    } else {
        token = req.get('Authorization');
        token = token.split(' ')[1];
        if (!token || token === null || token === '') {
            return res.status(401).send('Missing Token');
        } else {
            try {
                var decodedToken = jwt.verify(token, secret);
            } catch(err) {
                return res.status(401).send('Token not valid');
            }
            models.User.findAll({where: {email: decodedToken.data.email }, plain: true }).then(function(result){
                if (result === null) {
                    return res.status(401).send('User not valid');
                }
                var hmac = crypto.createHmac('sha256', key);
                hmac.update(result.password, result.tokenSalt);
                var sign_hash = hmac.digest('binary');
                if (decodedToken.data.sign === sign_hash) {
                    next();
                } else {
                    return res.status(401).send('Token not valid');
                }
            })
            .catch(function(){
                return res.status(500).send('Contact administrator');
            });
        }
    }
};
