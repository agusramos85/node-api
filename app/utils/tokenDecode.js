'use strict';
var config = require('./../config');
var jwt = require('jsonwebtoken');
var secret = config.secret;

/**
 * Decode token
 * @param {Object} request
 * @param {Ojbect} token 
 */
module.exports = function tokenDecode(req) {
    var token = req.get('Authorization');
    token = token.split(' ')[1];
    return jwt.verify(token, secret);
};
