'use strict';
const nodemailer = require('nodemailer');

/**
 * Sends mails
 * @param {String} to
 * @param {String} subject
 * @param {String} body 
 */
exports.sendMail = function(to, subject, body) {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'ramos.agustin.possible@gmail.com',
            pass: 'Agus1985'
        }
    });

    let mailOptions = {
        from: 'ramos.agustin.possible@gmail.com',
        to: to,
        subject: subject,
        text: body
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
};
