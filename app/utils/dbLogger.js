'use strict';
var models = require('../models');
var tokenDecode = require('./../utils/tokenDecode');

/**
 * Log action in DB
 * @param {Object} request
 * @param {String} action
 * @return {Void} 
 */
module.exports = function dbLogger(req, action) {
    var user = tokenDecode(req);
    user = user.data.idUser;
    models.Log.create({idUser: user, action: action});
};
