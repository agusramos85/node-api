var express    = require('express');
// Start app requires
var app        = express();
var auth       = require('./app/utils/auth');
var bodyParser = require('body-parser');
var morgan     = require('morgan');
var routes     = require('./app/routes');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = 8080;

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Content-Type, Accept, Access-Control-Allow-Headers, Authorization');
    if (req.method === 'OPTIONS') {
        return res.send(200);
    } else {
        return next();
    }
});

app.use(auth);
app.use('/api', routes);

app.listen(port);
console.log('Listen on ' + port);
